<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|PT+Sans:400,700&subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <div class="navbar navbar-conf">
            <div class="container">
                <div class="navbar-header clearfix">
                    <div class="navbar-logo">
                        <img src="img/logo.png" alt="" class="img-responsive">
                    </div>
                    <div class="navbar-city dropdown">
                        <a href="#" class="city-current" data-toggle="dropdown">Екатеринбург <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <ul class="city-select dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><a href="#">Пермь</a></li>
                            <li><a href="#">Уфа</a></li>
                            <li><a href="#" class="active">Екатеринбург</a></li>
                            <li><a href="#">Ижевск</a></li>
                        </ul>
                    </div>

                    <span class="navbar-toggle" data-target=".navbar-nav"><i class="fa fa-navicon"></i></span>
                </div>
                <ul class="navbar-nav clearfix">
                    <li><a href="#">РАЗМЕРЫ</a></li>
                    <li><a href="#">ДОСТАВКА</a></li>
                    <li><a href="#">КОМПАНИЯМ</a></li>
                    <li><a href="#">ГРУППОВОЙ ЗАКАЗ</a></li>
                    <li><a href="#">О НАС</a></li>
                    <li><a href="#">ОТЗЫВЫ</a></li>
                    <li><a href="#">КОНСТРУКТОР</a></li>
                </ul>
            </div>
        </div>

        <header class="header-conf"></header>


        <section class="product" id="conf">
            <div class="container">
                <div class="product-row">

                    <div class="product-params">

                        <!-- Подбор параметров -->
                        <div class="params">
                            <form class="form-params" id="to-form">
                                <h2>Попробуй сам</h2>

                                <!-- ВУЗ, Тип толстовки -->
                                <div class="params-group">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label class="label">Твой ВУЗ:</label>
                                            <select class="form-select vuz-value" name="vuz-name">
                                                <option value="pnrpu" data-target=".pnrpu">ПНРПУ</option>
                                                <option value="prue" data-target=".prue">ПРУЕ</option>
                                                <option value="psic" data-target=".psic">ПСИАК</option>
                                                <option value="pspa" data-target=".pspa">ПСПА</option>
                                                <option value="psu" data-target=".psu">ПСУ</option>
                                            </select>

                                        </div>
                                        <div class="col-sm-7">
                                            <label class="label">Тип толстовки</label>
                                            <ul class="params-type">
                                                <li class="form-radio" data-target=".product-kangaroo">
                                                    <label>
                                                        <span class="icr-text">Кенгуру</span>
                                                        <input type="radio" name="params-type" value="params-type" checked>
                                                    </label>
                                                </li>
                                                <li class="form-radio" data-target=".product-kangaroo-two">
                                                    <label>
                                                        <span class="icr-text">Кенгуру на замке</span>
                                                        <input type="radio" name="params-type" value="params-type">
                                                    </label>
                                                </li>
                                                <li class="form-radio" data-target=".product-bomber">
                                                    <label>
                                                        <span class="icr-text">Бомбер</span>
                                                        <input type="radio" name="params-type" value="params-type">
                                                    </label>
                                                </li>
                                                <li class="form-radio" data-target=".product-sweatshirt">
                                                    <label>
                                                        <span class="icr-text">Свитшот</span>
                                                        <input type="radio" name="params-type" value="params-type">
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <!-- Факультет -->
                                <div class="params-group">
                                    <div class="faculty">
                                        <div class="form-checkbox" id="add-fuc">
                                            <label>
                                                <span class="icr-text">Добавить факультет:</span>
                                                <input type="checkbox" name="params-faculty-check" value="params-faculty-check">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="faculty-switch">
                                        <a href="#" class="product-rotate">
                                            <div class="rotate-icon">
                                                <div class="svg-responsive">
                                                    <?php include('svg/rotate.svg') ?>
                                                </div>
                                            </div>
                                            <span>Показать спину</span>
                                        </a>
                                        <div class="params-faculty">
                                            <select class="form-select select-faculty" name="faculty">
                                                <option value="nofac" data-target="nofac">Не выбран</option>
                                                <option value="biology" data-target=".biology">Биологический</option>
                                                <option value="chemistry" data-target=".chemistry">Химический</option>
                                                <option value="economics" data-target="economics">Экономический</option>
                                                <option value="geogr" data-target="geogr">Географический</option>
                                                <option value="geology" data-target="geology">Геологический </option>
                                                <option value="history" data-target="history">Исторический</option>
                                                <option value="lang" data-target="lang">Лингвистический</option>
                                                <option value="law" data-target="">Юридический</option>
                                                <option value="mechanics" data-target="mechanics">Механико-математический</option>
                                            </select>
                                        </div>
                                    </div>
                                </div><!-- -->

                                <!-- Цвет толстовки -->
                                <div class="params-group">
                                    <label class="label">Цвет толстовки:</label>
                                    <ul class="colors base-color">
                                        <li style="background: #c5c5c5" data-target=".gray" id="main-gray">
                                            <label>
                                                <input type="radio" name="base-colors" value="gray" checked>
                                            </label>
                                        </li>
                                        <li style="background: #000000" data-target=".black" id="main-black">
                                            <label>
                                                <input type="radio" name="base-colors" value="black">
                                            </label>
                                        </li>
                                        <li style="background: #ff0000" data-target=".red" id="main-red">
                                            <label>
                                                <input type="radio" name="base-colors" value="red">
                                            </label>
                                        </li>
                                        <li style="background: #ffffff" data-target=".white" id="main-white">
                                            <label>
                                                <input type="radio" name="base-colors" value="white">
                                            </label>
                                        </li>
                                    </ul>
                                </div><!-- -->

                                <!-- Цвет вышивки -->
                                <div class="params-group">
                                    <label class="label">Цвет вышивки:</label>
                                    <ul class="colors params-color">
                                        <li style="background: #045eaf" data-target="#045eaf">
                                            <label>
                                                <input type="radio" name="params-colors" value="#045eaf" checked>
                                            </label>
                                        </li>
                                        <li style="background: #37aaff" data-target="#37aaff">
                                            <label>
                                                <input type="radio" name="params-colors" value="#37aaff">
                                            </label>
                                        </li>
                                        <li style="background: #0d214b" data-target="#0d214b">
                                            <label>
                                                <input type="radio" name="params-colors" value="#0d214b">
                                            </label>
                                        </li>
                                        <li style="background: #7fd647" data-target="#7fd647">
                                            <label>
                                                <input type="radio" name="params-colors" value="#7fd647">
                                            </label>
                                        </li>
                                        <li style="background: #0d8d71" data-target="#0d8d71">
                                            <label>
                                                <input type="radio" name="params-colors" value="#0d8d71">
                                            </label>
                                        </li>


                                        <li style="background: #068a12" data-target="#068a12">
                                            <label>
                                                <input type="radio" name="params-colors" value="#068a12">
                                            </label>
                                        </li>
                                        <li style="background: #28a9d3" data-target="#28a9d3">
                                            <label>
                                                <input type="radio" name="params-colors" value="#28a9d3">
                                            </label>
                                        </li>
                                        <li style="background: #2096a0" data-target="#2096a0">
                                            <label>
                                                <input type="radio" name="params-colors" value="#2096a0">
                                            </label>
                                        </li>
                                        <li style="background: #fbe422" data-target="#fbe422">
                                            <label>
                                                <input type="radio" name="params-colors" value="#fbe422">
                                            </label>
                                        </li>
                                        <li style="background: #ffae3d" data-target="#ffae3d">
                                            <label>
                                                <input type="radio" name="params-colors" value="#ffae3d">
                                            </label>
                                        </li>


                                        <li style="background: #cc3c79" data-target="#cc3c79">
                                            <label>
                                                <input type="radio" name="params-colors" value="#cc3c79">
                                            </label>
                                        </li>
                                        <li style="background: #86158d" data-target="#86158d">
                                            <label>
                                                <input type="radio" name="params-colors" value="#86158d">
                                            </label>
                                        </li>
                                        <li style="background: #e55d9d" data-target="#e55d9d">
                                            <label>
                                                <input type="radio" name="params-colors" value="#e55d9d">
                                            </label>
                                        </li>
                                        <li style="background: #a86e97" data-target="#a86e97">
                                            <label>
                                                <input type="radio" name="params-colors" value="#a86e97">
                                            </label>
                                        </li>
                                        <li style="background: #c12a24" data-target="#c12a24">
                                            <label>
                                                <input type="radio" name="params-colors" value="#c12a24">
                                            </label>
                                        </li>


                                        <li style="background: #f38626" data-target="#f38626">
                                            <label>
                                                <input type="radio" name="params-colors" value="#f38626">
                                            </label>
                                        </li>
                                        <li style="background: #f86a27" data-target="#f86a27">
                                            <label>
                                                <input type="radio" name="params-colors" value="#f86a27">
                                            </label>
                                        </li>
                                        <li style="background: #fe754c" data-target="#fe754c">
                                            <label>
                                                <input type="radio" name="params-colors" value="#fe754c">
                                            </label>
                                        </li>
                                        <li style="background: #ff4741" data-target="#ff4741">
                                            <label>
                                                <input type="radio" name="params-colors" value="#ff4741">
                                            </label>
                                        </li>
                                        <li style="background: #ff2200" data-target="#ff2200">
                                            <label>
                                                <input type="radio" name="params-colors" value="#ff2200">
                                            </label>
                                        </li>



                                        <li style="background: #665c5a" data-target="#665c5a">
                                            <label>
                                                <input type="radio" name="params-colors" value="#665c5a">
                                            </label>
                                        </li>
                                        <li style="background: #333333" data-target="#333333">
                                            <label>
                                                <input type="radio" name="params-colors" value="#333333">
                                            </label>
                                        </li>
                                        <li style="background: #ffffff" data-target="#ffffff">
                                            <label>
                                                <input type="radio" name="params-colors" value="#ffffff">
                                            </label>
                                        </li>
                                    </ul>
                                </div><!-- -->

                                <!-- Цвет обводки -->
                                <div class="params-group">
                                    <label class="label">Цвет обводки:</label>
                                    <ul class="colors params-stitch">
                                        <li style="background: #045eaf" data-target="#045eaf">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#045eaf">
                                            </label>
                                        </li>
                                        <li style="background: #37aaff" data-target="#37aaff">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#37aaff">
                                            </label>
                                        </li>
                                        <li style="background: #0d214b" data-target="#0d214b">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#0d214b">
                                            </label>
                                        </li>
                                        <li style="background: #7fd647" data-target="#7fd647">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#7fd647">
                                            </label>
                                        </li>
                                        <li style="background: #0d8d71" data-target="#0d8d71">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#0d8d71">
                                            </label>
                                        </li>


                                        <li style="background: #068a12" data-target="#068a12">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#068a12">
                                            </label>
                                        </li>
                                        <li style="background: #28a9d3" data-target="#28a9d3">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#28a9d3">
                                            </label>
                                        </li>
                                        <li style="background: #2096a0" data-target="#2096a0">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#2096a0">
                                            </label>
                                        </li>
                                        <li style="background: #fbe422" data-target="#fbe422">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#fbe422">
                                            </label>
                                        </li>
                                        <li style="background: #ffae3d" data-target="#ffae3d">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#ffae3d">
                                            </label>
                                        </li>


                                        <li style="background: #cc3c79" data-target="#cc3c79">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#cc3c79">
                                            </label>
                                        </li>
                                        <li style="background: #86158d" data-target="#86158d">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#86158d">
                                            </label>
                                        </li>
                                        <li style="background: #e55d9d" data-target="#e55d9d">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#e55d9d">
                                            </label>
                                        </li>
                                        <li style="background: #a86e97" data-target="#a86e97">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#a86e97">
                                            </label>
                                        </li>
                                        <li style="background: #c12a24" data-target="#c12a24">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#c12a24">
                                            </label>
                                        </li>


                                        <li style="background: #f38626" data-target="#f38626">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#f38626">
                                            </label>
                                        </li>
                                        <li style="background: #f86a27" data-target="#f86a27">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#f86a27">
                                            </label>
                                        </li>
                                        <li style="background: #fe754c" data-target="#fe754c">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#fe754c">
                                            </label>
                                        </li>
                                        <li style="background: #ff4741" data-target="#ff4741">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#ff4741">
                                            </label>
                                        </li>
                                        <li style="background: #ff2200" data-target="#ff2200">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#ff2200">
                                            </label>
                                        </li>



                                        <li style="background: #665c5a" data-target="#665c5a">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#665c5a">
                                            </label>
                                        </li>
                                        <li style="background: #333333" data-target="#333333">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#333333">
                                            </label>
                                        </li>
                                        <li style="background: #ffffff" data-target="#ffffff">
                                            <label>
                                                <input type="radio" name="params-stitch" value="#ffffff" checked>
                                            </label>
                                        </li>
                                    </ul>
                                </div><!-- -->

                                <!-- Размеры -->
                                <div class="params-group">
                                    <label class="label">Размер:</label>
                                    <ul class="params-size">
                                        <li>
                                            <label>
                                                <span class="icr-text">XXS</span>
                                                <input type="radio" name="params-params-size" value="XXS">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">XS</span>
                                                <input type="radio" name="params-params-size" value="XS">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">S</span>
                                                <input type="radio" name="params-params-size" value="S">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">M</span>
                                                <input type="radio" name="params-params-size" value="M">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">L</span>
                                                <input type="radio" name="params-params-size" value="L">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">XL</span>
                                                <input type="radio" name="params-params-size" value="XL">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">XXL</span>
                                                <input type="radio" name="params-params-size" value="XXL">
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <span class="icr-text">3XL</span>
                                                <input type="radio" name="params-params-size" value="3XL">
                                            </label>
                                        </li>
                                    </ul>
                                    <a href="#table-size" class="btn-modal btn-table">Таблица размеров</a>
                                </div><!-- -->

                                <ul class="params-footer clearfix">
                                    <li>
                                        <span class="total">2 250</span>. —
                                    </li>
                                    <li>
                                        <button type="submit" class="btn  btn-order">Заказать</button>
                                    </li>
                                </ul><!-- -->

                            </form>
                        </div>

                        <!-- Оформление заказа-->
                        <div class="order">
                            <form class="form" id="data-form">
                                <h2>
                                    <span class="product-name">Толстовка с молнией</span> <span class="price">2 250.—</span>
                                </h2>

                                <input type="hidden" name="order-product" value="Толстовка с молнией">

                                <div class="form-group">
                                    <ul class="order-result">
                                        <li class="order-base-color">
                                            <span>Цвет толстовки</span>
                                            <input type="text" name="order-base-color" value="c5c5c5" style="background: #c5c5c5" id="order-base-color">
                                        </li>
                                        <li class="order-image-color">
                                            <span>Цвет вышивки</span>
                                            <input type="text" name="order-image-color" value="665c5a" style="background: #665c5a" id="order-image-color">
                                        </li>
                                        <li class="order-stitch-color">
                                            <span>Цвет обводки</span>
                                            <input type="text" name="order-stitch-color" value="665c5a" style="background: #37aaff"id="order-stitch-color">
                                        </li>
                                        <li class="order-size">
                                            <span>Размер</span>
                                            <input type="text" name="order-size" value="L" id="order-size">
                                        </li>
                                    </ul>
                                </div>

                                <div class="form-group faculty-check">
                                    <div class="form-checkbox">
                                        <label>
                                            <span class="icr-text">Факультет: <span class="order-faculty">Не указан</span></span>
                                            <input type="checkbox" name="order-faculty-check" value="Не указан" checked disabled>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Имя и фамилия</label>
                                            <input type="text" name="order-name" class="form-control" placeholder="Саша Арбузов">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Электронная почта</label>
                                            <input type="text" name="order-email" class="form-control" placeholder="Campus@example.com">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Телефон</label>
                                            <input type="text" name="order-phone" class="form-control" placeholder="+7 (___) ___-__-__">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Способ оплаты</label>
                                            <select class="form-select" name="order-payment">
                                                <option value="pay_site">На сайте</option>
                                                <option value="pay_cash">Курьеру наличными</option>
                                                <option value="pay_post">Наложный платеж</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Город</label>
                                            <select class="form-select" name="order-city">
                                                <option value="pay_site">Екатеринбург</option>
                                                <option value="pay_cash">Перьмь</option>
                                                <option value="pay_post">Уфа</option>
                                                <option value="pay_post">Ижевск</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Код скидки</label>
                                            <div class="sale-box">
                                                <input type="text" name="order-sale" class="form-control" placeholder="_______">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="label">Доставка</label>
                                            <ul class="delivery-row clearfix">
                                                <li>
                                                    <select class="form-select" name="order-delivery">
                                                        <option value="delivery_01">Курьером</option>
                                                        <option value="delivery_02">Почтой России</option>
                                                        <option value="delivery_03">Самовывоз</option>
                                                    </select>
                                                </li>
                                                <li><span class="delivery-price">300</span> р.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <label class="label">Улица</label>
                                                    <input type="text" name="order-street" class="form-control" placeholder="">
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="label">Дом</label>
                                                    <input type="text" name="order-house" class="form-control" placeholder="">
                                                </div>
                                                <div class="col-xs-4">
                                                    <label class="label">Офис</label>
                                                    <input type="text" name="order-room" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="order-text">
                                    <p>Доставка оплачивается после получения заказа.</p>
                                    <p>Срок изготовления халата 7-10 дней. После принятия заказа и полаты халата придет СМС со всей информацией.</p>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <button type="submit" class="btn">Оформить заказ</button>
                                    </div>
                                    <div class="col-xs-6">
                                        <button class="btn btn-transparent btn-back">Вернуться назад</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="product-visual">

                        <div class="product-conf">
                            <div class="product-title-sm title-conf">Попробуй сам</div>

                            <!-- Кенгуру -->
                            <div class="product-type product-kangaroo active">

                                <div class="product-view product-front">
                                    <div class="product-color white">
                                        <img src="images/configurator/kangaroo/white-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/kangaroo/gray-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/kangaroo/gray-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/kangaroo/black-front.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #045eaf; fill-rule:nonzero;">
                                        <div class="product-print-item svg-responsive pnrpu active">
                                            <?php include('svg/kangaroo/pnrpu.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive prue">
                                            <?php include('svg/kangaroo/prue.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psic">
                                            <?php include('svg/kangaroo/psic.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive pspa">
                                            <?php include('svg/kangaroo/pspa.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psu">
                                            <?php include('svg/kangaroo/psu.svg') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-view product-back">
                                    <div class="product-color white">
                                        <img src="images/configurator/kangaroo/white-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/kangaroo/gray-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/kangaroo/gray-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/kangaroo/black-back.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-back-print-item svg-responsive biology active">
                                            <?php include('svg/faculty/biology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive chemistry">
                                            <?php include('svg/faculty/chemistry.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive economics">
                                            <?php include('svg/faculty/economics.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geogr">
                                            <?php include('svg/faculty/geogr.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geology">
                                            <?php include('svg/faculty/geology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive history">
                                            <?php include('svg/faculty/history.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive lang">
                                            <?php include('svg/faculty/lang.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive law">
                                            <?php include('svg/faculty/law.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive mechanics">
                                            <?php include('svg/faculty/mechanics.svg') ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- -->

                            <!-- Кенгуру на замке-->
                            <div class="product-type product-kangaroo-two">

                                <div class="product-view product-front">
                                    <div class="product-color white">
                                        <img src="images/configurator/kangaroo/white-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/kangaroo/gray-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/kangaroo/gray-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/kangaroo/black-front.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-print-item svg-responsive pnrpu active">
                                            <?php include('svg/bomber/pnrpu.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive prue">
                                            <?php include('svg/bomber/prue.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psic">
                                            <?php include('svg/bomber/psic.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive pspa">
                                            <?php include('svg/bomber/pspa.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psu">
                                            <?php include('svg/bomber/psu2.svg') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-view product-back">
                                    <div class="product-color white">
                                        <img src="images/configurator/kangaroo/white-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/kangaroo/gray-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/kangaroo/gray-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/kangaroo/black-back.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-back-print-item svg-responsive biology active">
                                            <?php include('svg/faculty/biology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive chemistry">
                                            <?php include('svg/faculty/chemistry.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive economics">
                                            <?php include('svg/faculty/economics.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geogr">
                                            <?php include('svg/faculty/geogr.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geology">
                                            <?php include('svg/faculty/geology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive history">
                                            <?php include('svg/faculty/history.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive lang">
                                            <?php include('svg/faculty/lang.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive law">
                                            <?php include('svg/faculty/law.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive mechanics">
                                            <?php include('svg/faculty/mechanics.svg') ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- -->

                            <!-- Бомбер -->
                            <div class="product-type product-bomber">

                                <div class="product-view product-front active">
                                    <div class="product-color black active">
                                        <img src="images/configurator/bomber/black-front.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/bomber/red-front.jpg" alt="" class="img-responsive">
                                    </div>


                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-print-item svg-responsive pnrpu active">
                                            <?php include('svg/bomber/pnrpu.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive prue">
                                            <?php include('svg/bomber/prue.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psic">
                                            <?php include('svg/bomber/psic.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive pspa">
                                            <?php include('svg/bomber/pspa.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psu">
                                            <?php include('svg/bomber/psu2.svg') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-view product-back">
                                    <div class="product-color black active">
                                        <img src="images/configurator/bomber/black-back.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/bomber/red-back.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-back-print-item svg-responsive biology active">
                                            <?php include('svg/faculty/biology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive chemistry">
                                            <?php include('svg/faculty/chemistry.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive economics">
                                            <?php include('svg/faculty/economics.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geogr">
                                            <?php include('svg/faculty/geogr.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geology">
                                            <?php include('svg/faculty/geology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive history">
                                            <?php include('svg/faculty/history.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive lang">
                                            <?php include('svg/faculty/lang.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive law">
                                            <?php include('svg/faculty/law.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive mechanics">
                                            <?php include('svg/faculty/mechanics.svg') ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- -->

                            <!-- Свитшот -->
                            <div class="product-type product-sweatshirt">

                                <div class="product-view product-front active">
                                    <div class="product-color white">
                                        <img src="images/configurator/sweatshirt/front-white.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/sweatshirt/front-gray.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/sweatshirt/front-gray.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/sweatshirt/front-black.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24; fill-rule:nonzero;">
                                        <div class="product-print-item svg-responsive pnrpu active">
                                            <?php include('svg/kangaroo/pnrpu.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive prue">
                                            <?php include('svg/kangaroo/prue.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psic">
                                            <?php include('svg/kangaroo/psic.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive pspa">
                                            <?php include('svg/kangaroo/pspa.svg') ?>
                                        </div>
                                        <div class="product-print-item svg-responsive psu">
                                            <?php include('svg/kangaroo/psu.svg') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-view product-back">
                                    <div class="product-color white">
                                        <img src="images/configurator/sweatshirt/back-white.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color gray active">
                                        <img src="images/configurator/sweatshirt/back-gray.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color red">
                                        <img src="images/configurator/sweatshirt/back-gray.jpg" alt="" class="img-responsive">
                                    </div>
                                    <div class="product-color black">
                                        <img src="images/configurator/sweatshirt/back-black.jpg" alt="" class="img-responsive">
                                    </div>

                                    <div class="product-print" style="fill: #E31E24;">
                                        <div class="product-back-print-item svg-responsive biology active">
                                            <?php include('svg/faculty/biology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive chemistry">
                                            <?php include('svg/faculty/chemistry.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive economics">
                                            <?php include('svg/faculty/economics.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geogr">
                                            <?php include('svg/faculty/geogr.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive geology">
                                            <?php include('svg/faculty/geology.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive history">
                                            <?php include('svg/faculty/history.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive lang">
                                            <?php include('svg/faculty/lang.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive law">
                                            <?php include('svg/faculty/law.svg') ?>
                                        </div>
                                        <div class="product-back-print-item svg-responsive mechanics">
                                            <?php include('svg/faculty/mechanics.svg') ?>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- -->
                        </div>

                        <div class="order-map">
                            <h2>Выберете место откуда хотите забрать толстовку</h2>
                            <div id="map">

                            </div>
                            <p>Вы выбрали самомувывоз с <span class="place">Ул. Малышева, 6</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <div class="content">
                    <h2>В чем магия толстовки Campus?</h2>
                    <p>Помнишь, как круто в своих толстовках выглядят студенты Оксфорда, Гарварда, Кембриджа в американских фильмах? Это символ гордости за универ, за себя. </p>
                    <p>Это идеальная одежда для студенческих тусовок, сессий, спорта, прогулок.
                        И ты сразу понравишься преподам. </p>
                    <p>Толстовка прослужит тебе на протяжении всего студенчества.
                        Будет с тобой в самые сочные моменты.</p>
                    <a href="#conf" class="btn btn-purple btn-scroll">Да, я хочу толстовку</a>
                </div>

                <div class="footer-bottom clearfix">
                    <ul class="footer-one clearfix">
                        <li>
                            <img src="img/visa-m-c.png" alt="" class="img-responsive">
                        </li>
                        <li>© 2014-2016 All rights reserved — Campus</li>
                    </ul>

                    <ul class="footer-social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                    <a href="#" class="footer-dev svg-responsive">
                        <img src="img/logo-footer.svg" alt="">
                    </a>
                </div>
            </div>
        </footer>

        <!-- Таблица размеров -->
        <div class="hide">
            <div class="modal-table" id="table-size">
                <div class="tabs">
                    <ul class="tabs-nav clearfix">
                        <li class="active"><a href="#" data-target=".tab1">Кенгуру</a></li>
                        <li class="active"><a href="#" data-target=".tab2">Кенгуру на замке</a></li>
                        <li><a href="#" data-target=".tab3">Бомбер</a></li>
                        <li><a href="#" data-target=".tab4">Свитшот</a></li>
                    </ul>
                    <div class="tabs-content">
                        <div class="tabs-item tab1 active">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Размер</th>
                                        <th>Ширина плеч</th>
                                        <th>Длина</th>
                                        <th>Ширина груди</th>
                                        <th>Длина рукава</th>
                                    </tr>
                                    <tr>
                                        <td>XXS</td>
                                        <td>40</td>
                                        <td>65</td>
                                        <td>61</td>
                                        <td>48</td>
                                    </tr>
                                    <tr>
                                        <td>XS</td>
                                        <td>42</td>
                                        <td>66</td>
                                        <td>62.5</td>
                                        <td>50</td>
                                    </tr>
                                    <tr>
                                        <td>S</td>
                                        <td>44</td>
                                        <td>67.5</td>
                                        <td>64</td>
                                        <td>53</td>
                                    </tr>
                                    <tr>
                                        <td>M</td>
                                        <td>46</td>
                                        <td>69</td>
                                        <td>64.5</td>
                                        <td>56</td>
                                    </tr>
                                    <tr>
                                        <td>L</td>
                                        <td>48</td>
                                        <td>70.5</td>
                                        <td>65</td>
                                        <td>59</td>
                                    </tr>
                                    <tr>
                                        <td>XL</td>
                                        <td>50</td>
                                        <td>72</td>
                                        <td>65.5</td>
                                        <td>62</td>
                                    </tr>
                                    <tr>
                                        <td>XXL</td>
                                        <td>52</td>
                                        <td>73.5</td>
                                        <td>70</td>
                                        <td>64</td>
                                    </tr>
                                    <tr>
                                        <td>3XL</td>
                                        <td>54</td>
                                        <td>75</td>
                                        <td>71</td>
                                        <td>66</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tabs-item tab2">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Размер</th>
                                        <th>Ширина плеч</th>
                                        <th>Длина</th>
                                        <th>Ширина груди</th>
                                        <th>Длина рукава</th>
                                    </tr>
                                    <tr>
                                        <td>XXS</td>
                                        <td>40</td>
                                        <td>65</td>
                                        <td>61</td>
                                        <td>48</td>
                                    </tr>
                                    <tr>
                                        <td>XS</td>
                                        <td>42</td>
                                        <td>66</td>
                                        <td>62.5</td>
                                        <td>50</td>
                                    </tr>
                                    <tr>
                                        <td>S</td>
                                        <td>44</td>
                                        <td>67.5</td>
                                        <td>64</td>
                                        <td>53</td>
                                    </tr>
                                    <tr>
                                        <td>M</td>
                                        <td>46</td>
                                        <td>69</td>
                                        <td>64.5</td>
                                        <td>56</td>
                                    </tr>
                                    <tr>
                                        <td>L</td>
                                        <td>48</td>
                                        <td>70.5</td>
                                        <td>65</td>
                                        <td>59</td>
                                    </tr>
                                    <tr>
                                        <td>XL</td>
                                        <td>50</td>
                                        <td>72</td>
                                        <td>65.5</td>
                                        <td>62</td>
                                    </tr>
                                    <tr>
                                        <td>XXL</td>
                                        <td>52</td>
                                        <td>73.5</td>
                                        <td>70</td>
                                        <td>64</td>
                                    </tr>
                                    <tr>
                                        <td>3XL</td>
                                        <td>54</td>
                                        <td>75</td>
                                        <td>71</td>
                                        <td>66</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tabs-item tab3">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Размер</th>
                                        <th>Ширина плеч</th>
                                        <th>Длина</th>
                                        <th>Ширина груди</th>
                                        <th>Длина рукава</th>
                                    </tr>
                                    <tr>
                                        <td>XXS</td>
                                        <td>40</td>
                                        <td>65</td>
                                        <td>61</td>
                                        <td>48</td>
                                    </tr>
                                    <tr>
                                        <td>XS</td>
                                        <td>42</td>
                                        <td>66</td>
                                        <td>62.5</td>
                                        <td>50</td>
                                    </tr>
                                    <tr>
                                        <td>S</td>
                                        <td>44</td>
                                        <td>67.5</td>
                                        <td>64</td>
                                        <td>53</td>
                                    </tr>
                                    <tr>
                                        <td>M</td>
                                        <td>46</td>
                                        <td>69</td>
                                        <td>64.5</td>
                                        <td>56</td>
                                    </tr>
                                    <tr>
                                        <td>L</td>
                                        <td>48</td>
                                        <td>70.5</td>
                                        <td>65</td>
                                        <td>59</td>
                                    </tr>
                                    <tr>
                                        <td>XL</td>
                                        <td>50</td>
                                        <td>72</td>
                                        <td>65.5</td>
                                        <td>62</td>
                                    </tr>
                                    <tr>
                                        <td>XXL</td>
                                        <td>52</td>
                                        <td>73.5</td>
                                        <td>70</td>
                                        <td>64</td>
                                    </tr>
                                    <tr>
                                        <td>3XL</td>
                                        <td>54</td>
                                        <td>75</td>
                                        <td>71</td>
                                        <td>66</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tabs-item tab4">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Размер</th>
                                        <th>Ширина плеч</th>
                                        <th>Длина</th>
                                        <th>Ширина груди</th>
                                        <th>Длина рукава</th>
                                    </tr>
                                    <tr>
                                        <td>XXS</td>
                                        <td>40</td>
                                        <td>65</td>
                                        <td>61</td>
                                        <td>48</td>
                                    </tr>
                                    <tr>
                                        <td>XS</td>
                                        <td>42</td>
                                        <td>66</td>
                                        <td>62.5</td>
                                        <td>50</td>
                                    </tr>
                                    <tr>
                                        <td>S</td>
                                        <td>44</td>
                                        <td>67.5</td>
                                        <td>64</td>
                                        <td>53</td>
                                    </tr>
                                    <tr>
                                        <td>M</td>
                                        <td>46</td>
                                        <td>69</td>
                                        <td>64.5</td>
                                        <td>56</td>
                                    </tr>
                                    <tr>
                                        <td>L</td>
                                        <td>48</td>
                                        <td>70.5</td>
                                        <td>65</td>
                                        <td>59</td>
                                    </tr>
                                    <tr>
                                        <td>XL</td>
                                        <td>50</td>
                                        <td>72</td>
                                        <td>65.5</td>
                                        <td>62</td>
                                    </tr>
                                    <tr>
                                        <td>XXL</td>
                                        <td>52</td>
                                        <td>73.5</td>
                                        <td>70</td>
                                        <td>64</td>
                                    </tr>
                                    <tr>
                                        <td>3XL</td>
                                        <td>54</td>
                                        <td>75</td>
                                        <td>71</td>
                                        <td>66</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- -->


        <!-- Спасибо -->
            <div class="hide">
                <div class="modal modal-valid" id="valid">
                    <div class="modal-content">
                        <div class="modal-thanks">Спасибо за покупку. Теперь ты в команде Campus!<br/>Жди смс-подтверждение. </div>
                        <a href="#">#getcampus</a>
                        <a href="#">#гордисьуниверситетом</a>
                        <a href="#">#снимайтолстовкувовремясекса</a>
                    </div>
                </div>
            </div>
        <!-- -->

        <!-- Ошибка -->
        <div class="hide">
            <div class="modal modal-invalid" id="invalid">
                <div class="modal-content">
                    <div class="modal-thanks">Что-то пошло не так, попробуйте перезагрузить страницу или напишите нам на <a href="mailto:allincampus@gmail.com">allincampus@gmail.com</a></div>
                </div>
            </div>
        </div>
        <!-- -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.pack.js"></script>
        <script src="js/vendor/ion.checkRadio.min.js"></script>
        <script src="js/vendor/jquery.ikSelect.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/instafeed.min.js"></script>
        <script src="js/vendor/dropdown.js"></script>
        <script src="js/main.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    </body>
</html>
