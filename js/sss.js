// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});

$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>',
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});

$('.btn-scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: 0});
    return false;
});


$('.header-slider').slick({
    dots: false,
    arrows: true,
    loop: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="header-nav prev"><i class="fa fa-caret-left" aria-hidden="true"></i></span>',
    nextArrow: '<span class="header-nav next"><i class="fa fa-caret-right" aria-hidden="true"></i></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                dots: false
            }
        }
    ]
});



$('.adv-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

// Чекбоксы

$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();


// tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


/* Конфигуратор */
/* ----------- */

$('#add-fuc label').click(function(){
    if(!$(this).hasClass('checked')){
        $('.product-rotate,.params-faculty').css('display','inline-block');
    }else{
        $('.product-rotate,.params-faculty').css('display','none');
        $('.product-type').removeClass('rotate');
        $('.product-rotate span').text('Показать спину');
    }

});

$('.params-size li').click(function(){
    $('.params-size').removeClass('my-error');
    var value = val = $(this).find('input').val();
    $('#order-size').val(value);

});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function isPhone(email) {
    var regex = /((8|\+7)-?)?\(?\d{3}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}/;
    return regex.test(email);
}
function check_rotate_text(){
    if($('.product-rotate span').text()=='Показать спину')
        $('.product-rotate span').text('Показать грудь');
    else
        $('.product-rotate span').text('Показать спину');
}


$('#data-form').submit(function(){
    var err=false;
    $('input[name="order-name"],input[name="order-email"],input[name="order-phone"],input[name="order-street"],input[name="order-house"]').removeClass('my-error');
    if(!$('input[name="order-name"]').val()){
        $('input[name="order-name"]').addClass('my-error');
        err=true;
    }
    if(!isEmail($('input[name="order-email"]').val())){
        $('input[name="order-email"]').addClass('my-error');
        err=true;
    }
    if(!isPhone($('input[name="order-phone"]').val())){
        $('input[name="order-phone"]').addClass('my-error');
        err=true;
    }
    deliv_meth=$('select[name="order-delivery"]').val();
    if(deliv_meth=='delivery_01' || deliv_meth=='delivery_02'){
        if(!$('input[name="order-street"]').val()){
            $('input[name="order-street"]').addClass('my-error');
            err=true;
        }
        if(!$('input[name="order-house"]').val()){
            $('input[name="order-house"]').addClass('my-error');
            err=true;
        }
    }
    if(err) return false;
    alert('all good');
});

$('select[name="order-delivery"]').on('change',function(){
    if($(this).val()=='delivery_03'){
        $('.product-conf').css('display','none');
        $('.order-map').css('display','block');
    }else{
        $('.product-conf').css('display','block');
        $('.order-map').css('display','none');
    }

});


// Тип толстовки


$('.params-type li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    container.find('.product-type').removeClass('active');
    container.find(value).addClass('active');
    if(value=='.product-bomber')
        $('#main-gray,#main-white').hide();
    else
        $('#main-gray,#main-white').show();
});


$('.product-rotate').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product');
    container.find('.product-type').toggleClass('rotate');
    check_rotate_text();
});



// Цвет толстовки

$('.base-color li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  ($(this).attr("data-target"));
    var color = $(this).css('background-color');
    var val = $(this).find('input').val();
    container.find('.product-color').removeClass('active');
    container.find(value).addClass('active');
    console.log(val);
    $('#order-base-color').removeAttr('style').css('background-color',color).val(val);
});

// Цвет вышивки

$('.params-color li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    var color = $(this).css('background-color');
    var val = $(this).find('input').val();
    container.find('.fil0').css("fill", value);
    $('#order-image-color').removeAttr('style').css('background-color',color).val(val);
});

// Цвет вышивки

$('.params-stitch li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    var val = $(this).find('input').val();
    var color = $(this).css('background-color');
    container.find('.str0').css("stroke", value);
    var color = $(this).css('background-color');
    $('#order-stitch-color').removeAttr('style').css('background-color',color).val(val);
});


// Название ВУЗа

var $params = $(".form-params");

$params.on("change", function () {
    var value = '.' + $('.vuz-value').val();

    console.log("Value: " + value);
    var container = $('.product');
    container.find('.product-print-item').removeClass('active');
    container.find(value).addClass('active');
});



$('.btn-order, .btn-back').click(function(e) {
    e.preventDefault();
    if($(this).hasClass('btn-back')){
        $('.product-conf').css('display','block');
        $('.order-map').css('display','none');
    }
    if($('.params-size .checked').length==1){
        var container = $(this).closest('.product');
        container.toggleClass('checkout');
    }else{
        $('.params-size').addClass('my-error');
    }
});