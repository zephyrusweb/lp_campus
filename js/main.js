// Navs


$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});

$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>',
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});

$(".btn-modal-two").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close-two" href="javascript:;"></a>',
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin fancybox-transparent"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});

$('.btn-scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: 0});
    return false;
});

$('.header-slider').slick({
    dots: true,
    arrows: true,
    loop: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="header-nav prev"><i class="fa fa-caret-left" aria-hidden="true"></i></span>',
    nextArrow: '<span class="header-nav next"><i class="fa fa-caret-right" aria-hidden="true"></i></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                dots: false
            }
        }
    ]
});




$('.adv-slider').slick({
    dots: true,
    arrows: false,
    loop: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

// Чекбоксы

$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();

$(function () {
    $('select').ikSelect({
        autoWidth: false,
        ddFullWidth: false,
        dynamicWidth: false,
        equalWidths: true,
        extractLink: false,
        linkCustomClass: '',
        ddCustomClass: '',
        filter: false,
        ddMaxHeight: 300,
        customClass: 'form-select'
    });
});


// tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


/* Конфигуратор */
/* ----------- */


// Тип толстовки


$('.params-type li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    container.find('.product-type').removeClass('active');
    container.find(value).addClass('active');
    var prName = $(this).find('.icr-text').text();

    console.log(prName);

    $('.product-name').text(prName);

    if(value=='.product-bomber') {
        $('#main-gray,#main-white').hide();
		if($('#main-gray .icr-label.checked').length > 0 || $('#main-white .icr-label.checked').length > 0){
			$('.product-params').find('.base-color .icr-label.checked').removeClass('checked');
			$('.product-params').find('#main-black >label').addClass('checked');
			$('#main-black input').prop("checked", true);
				var el=$('#main-black'); 
				var container = el.closest('.product');
				var value =  (el.attr("data-target"));
				var color = el.css('background-color');
				var val = el.find('input').val();
				container.find('.product-color').removeClass('active');
				container.find(value).addClass('active');
				console.log(val);
				$('#order-base-color').removeAttr('style').css('background-color',color).val(val);
				
		}
       // $('#main-black input').prop("checked", true);
        //$('#order-base-color').removeAttr('style').css('background-color','#000').val('#000');
    }
    else {
        $('#main-gray,#main-white').show();
    }
});


$('.product-rotate').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product');
    container.find('.product-type').toggleClass('rotate');
    check_rotate_text();
});


// Цвет толстовки

$('.base-color li').click(function(e) {
	$('.product-params').find('.base-color .icr-label.checked').removeClass('checked');
	$(this).find('.icr-label').addClass('checked');
    var container = $(this).closest('.product');
    var value =  ($(this).attr("data-target"));
    var color = $(this).css('background-color');
    var val = $(this).find('input').val();
    container.find('.product-color').removeClass('active');
    container.find(value).addClass('active');
    console.log(val);
    $('#order-base-color').removeAttr('style').css('background-color',color).val(val);
});

// Цвет вышивки

$('.params-color li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    var color = $(this).css('background-color');
    var val = $(this).find('input').val();
    container.find('.fil0').css("fill", value);
    $('#order-image-color').removeAttr('style').css('background-color',color).val(val);
});

// Цвет обводки

$('.params-stitch li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    var val = $(this).find('input').val();
    var color = $(this).css('background-color');
    container.find('.str0').css("stroke", value);
    $('#order-stitch-color').removeAttr('style').css('background-color',color).val(val);
});


// Название ВУЗа

var $params = $(".form-params");

$params.on("change", function () {
    var value = '.' + $('.vuz-value').val();
    var faculty = '.' + $('.select-faculty').val();
    var container = $('.product');
    var facName;

    switch (faculty) {
        case '.nofac' : facName = 'Не выбран';
            break;
        case '.biology' : facName = 'Биологичекий';
            break;
        case '.chemistry' : facName = 'Химический';
            break;
        case '.economics' : facName = 'Экономический';
            break;
        case '.geogr' : facName = 'Географический';
            break;
        case '.geology' : facName = 'Геологический';
            break;
        case '.history' : facName = 'Исторический';
            break;
        case '.lang' : facName = 'Лингвистический';
            break;
        case '.law' : facName = 'Юридический';
            break;
        case '.mechanics' : facName = 'Механико-математический';
            break;
    }

    container.find('.product-print-item').removeClass('active');
    container.find(value).addClass('active');

    container.find('.product-back-print-item').removeClass('active');
    container.find(faculty).addClass('active');

    $('.faculty-check .order-faculty').text(facName);
    $('.faculty-check input').val(facName);
});


// -----------------------

$('#add-fuc label').click(function(){
    var checkbox = ($(this).find('input').is(':checked'));
    var fac = $('.select-faculty').val();


    if(checkbox){
        $('.faculty-switch').addClass('check');
        $('.select-faculty').prop("disabled", false);
        $('.faculty-check').addClass('check');
        $('.faculty-check .order-faculty').text(fac);
        $('.faculty-check input').val(fac);
        $('.product-type').addClass('rotate');
        $('.product-rotate span').text('Показать грудь');
    }else{
        $('.select-faculty').prop("disabled", true);
        $('.faculty-switch').removeClass('check');
        $('.product-type').removeClass('rotate');
        $('.product-rotate span').text('Показать спину');
        $('.faculty-check').removeClass('check');

        $('.faculty-check .order-faculty').text('Не указано');
        $('.faculty-check input').val('Не указано');
    }

});

$('.params-size li').click(function(){
    $('.params-size').removeClass('my-error');
    var value = val = $(this).find('input').val();
    $('#order-size').val(value);

});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function isPhone(email) {
    var regex = /((8|\+7)-?)?\(?\d{3}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}/;
    return regex.test(email);
}

function check_rotate_text(){
    if($('.product-rotate span').text()=='Показать спину')
        $('.product-rotate span').text('Показать грудь');
    else
        $('.product-rotate span').text('Показать спину');
}


$('#data-form').submit(function(){
    var err=false;
    $('input[name="order-name"],input[name="order-email"],input[name="order-phone"],input[name="order-street"],input[name="order-house"]').removeClass('my-error');

    if(!$('input[name="order-name"]').val()){
        $('input[name="order-name"]').addClass('my-error');
        err=true;
    }
    if(!isEmail($('input[name="order-email"]').val())){
        $('input[name="order-email"]').addClass('my-error');
        err=true;
    }
    if(!isPhone($('input[name="order-phone"]').val())){
        $('input[name="order-phone"]').addClass('my-error');
        err=true;
    }
    deliv_meth=$('select[name="order-delivery"]').val();
    if(deliv_meth=='delivery_01' || deliv_meth=='delivery_02'){
        if(!$('input[name="order-street"]').val()){
            $('input[name="order-street"]').addClass('my-error');
            err=true;
        }
        if(!$('input[name="order-house"]').val()){
            $('input[name="order-house"]').addClass('my-error');
            err=true;
        }
    }
    if(err) return false;

    else  {
        $.fancybox(
            jQuery('#valid').html(),
            {
                'padding'    : 0,
                'tpl'        : {
                    closeBtn : '<a title="Close" class="btn-close-two" href="javascript:;"></a>',
                    wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin fancybox-transparent"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
                }
            }
        );
    }
});

$('.btn-order, .btn-back').click(function(e) {
    e.preventDefault();
    if($(this).hasClass('btn-back')){
        $('.product-visual').removeClass('point');
    }
    if($('.params-size .checked').length==1){
        var container = $(this).closest('.product');
        container.toggleClass('checkout');
    }else{
        $('.params-size').addClass('my-error');
    }
});


$( ".order input" ).focus(function() {
    $( this ).removeClass('my-error');
});

$('select[name="order-delivery"]').on('change',function(){
    if($(this).val()=='delivery_03'){
        $('.product-visual').addClass('point');
        $(this).closest('.form-group').addClass('view-map');
    }else{
        $('.product-visual').removeClass('point');
        $(this).closest('.form-group').removeClass('view-map');
    }

});


// Map

ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
        center: [55.7434,37.8346],
        zoom: 15,
        controls: ['smallMapDefaultSet']
    });


    myMap.geoObjects
        .add(new ymaps.Placemark([55.7434,37.8346], {
            balloonContent: '<strong>Ул. Малышева, 6<br/><small>есть терминал</small></strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}

